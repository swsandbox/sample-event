package io.swsb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.zeromq.ZMQ;

import java.io.IOException;
import java.util.concurrent.Executors;

/**
 * Created by swsb.
 */
public class Events
{
    public static void main(String[] args)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket push = context.socket(ZMQ.PUSH);
        push.connect("tcp://localhost:9500");

        ZMQ.Socket pull = context.socket(ZMQ.PULL);
        pull.bind("tcp://localhost:9500");

        Runnable runnable = () -> {
            while (!Thread.currentThread().isInterrupted())
            {
                Event event = new Event();
                event.setType("registration");
                try
                {
                    System.out.println("pushing...");
                    push.send(objectMapper.writeValueAsString(event));
                }
                catch (JsonProcessingException e)
                {
                    e.printStackTrace();
                }
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        };

        System.out.println("starting pusher");
        Executors.newFixedThreadPool(1).submit(runnable);

        while (!Thread.currentThread().isInterrupted())
        {
            try
            {
                System.out.println("reading...");
                Event event = objectMapper.readValue(pull.recvStr(), Event.class);
                System.out.println("Event received: " + event);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }


        System.out.println("done...");
    }
}
