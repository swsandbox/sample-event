package io.swsb;

import java.util.Map;

/**
 * Created by swsb.
 */
public class Event
{
    private String type;
    private Map<String, Object> body;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, Object> getBody()
    {
        return body;
    }

    public void setBody(Map<String, Object> body)
    {
        this.body = body;
    }

    @Override
    public String toString()
    {
        return "Event{" +
                "type='" + type + '\'' +
                ", body=" + body +
                '}';
    }
}
